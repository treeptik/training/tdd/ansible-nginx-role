[![(c) Treeptik, Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

TDD Ansible NGINX role
(c) by Treeptik is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
You should have received a copy of the license along with this work.
If not, see http://creativecommons.org/licenses/by-sa/4.0/ .
