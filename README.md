# TDD Ansible NGINX role

[![(c) Treeptik, Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

Test Driven Development of installing and configuring NGINX with an Ansible role

## License

See [License](LICENSE.md)

## Problem

Install and configure an NGINX server that listens to port 80.

## Running Tests

```shell
pipenv install --dev
pipenv run molecule test
```
